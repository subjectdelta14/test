import { defineStore } from 'pinia';
import UserService from 'src/services/user';
import AuthService from 'src/services/auth';
import { removeToken, setToken } from 'src/helper/auth';

export interface State {
  lang: string;
  user?: any;
}

export const useMainStore = defineStore({
  id: 'mainStore',
  state: () =>
    ({
      lang: 'en',
      user: null,
    } as State),

  getters: {
    getLang: (state) => {
      return state.lang;
    },
    getUser: (state) => {
      return state.user;
    },
  },

  actions: {
    async changeLanguages(languages: string, that: any) {
      this.lang = languages;
      that.$i18n.locale = languages;
      console.log('languages', this.lang);
    },
    updateUser(user: any) {
      this.user = user;
    },
    getInfoUser() {
      UserService.profile()
        .then((res) => {
          console.log(res);
          this.updateUser(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    refreshToken(refresh_token: string) {
      AuthService.refreshToken({
        refresh_token,
      }).then((res) => {
        console.log(res);
        setToken(JSON.stringify(res.data));
      });
    },
    logout() {
      removeToken();
      AuthService.logout({})
        .then((res) => {
          console.log(res);
        })
        .catch((e) => {
          console.log(e);
        });
    },
  },
});
