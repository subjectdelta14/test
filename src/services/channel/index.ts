import { api } from 'boot/axios';
import {
  bodyAddMember,
  bodyChannel,
  bodyCreateOneToOne,
  bodyMuteChannel,
  bodyUpdateChannel,
  channelId,
  paramsFiles,
  paramsMember,
} from 'src/services/channel/model';
const channel = '/channel';
class ChannelService {
  createChannel(body: bodyChannel) {
    return api.post(`${channel}`, body);
  }

  getAllChannel(workspaceId: string) {
    return api.get(`${channel}/joined/${workspaceId}`);
  }

  getDetailChannel(id: string) {
    return api.get(`${channel}/${id}`);
  }

  editChannel(id: string, body: bodyUpdateChannel) {
    return api.put(`${channel}/${id}`, body);
  }

  findAllFileChannel(id: string, params: paramsFiles) {
    return api.get(`${channel}/${id}/files`, {
      params,
    });
  }

  findAllMemberChannel(id: string, params: paramsMember) {
    return api.get(`${channel}/${id}/members`, {
      params,
    });
  }

  addMemberChannel(body: bodyAddMember) {
    return api.post(`${channel}/add-member`, body);
  }

  removeMemberChannel(body: bodyAddMember) {
    return api.post(`${channel}/remove-member`, body);
  }

  leaveChannel(body: channelId) {
    return api.post(`${channel}/leave`, body);
  }

  muteChannel(body: bodyMuteChannel) {
    return api.post(`${channel}/mute`, body);
  }

  createChannelOnoToOne(body: bodyCreateOneToOne) {
    return api.post(`${channel}/one-to-one`, body);
  }

  closeChannelOnoToOne(body: channelId) {
    return api.post(`${channel}/one-to-one/close`, body);
  }
}

export default new ChannelService();
