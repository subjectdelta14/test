import { RouteRecordRaw } from 'vue-router';

const routesWorkspace: RouteRecordRaw[] = [
  {
    path: '/workspace/:workspaceId',
    name: 'workspace',
    component: () => import('layouts/LayoutWorkSpace.vue'),
    children: [
      {
        path: '',
        name: 'workspace-detail',
        component: () => import('pages/workspace/_workspaceId/index.vue'),
        meta: {
          requireAuth: true,
        },
      },
      {
        path: 'unreadTasks',
        name: 'unread-tasks',
        component: () => import('pages/workspace/_workspaceId/unreadTasks.vue'),
        meta: {
          requireAuth: true,
          title: 'Unread tasks',
          icon: 'file-text',
        },
      },
      {
        path: 'comment',
        name: 'comment',
        component: () => import('pages/workspace/_workspaceId/comment.vue'),
        meta: {
          requireAuth: true,
          title: 'Comments & Status',
          icon: 'chat-square-dots',
        },
      },
      {
        path: 'draft',
        name: 'draft',
        component: () => import('pages/workspace/_workspaceId/draft.vue'),
        meta: {
          requireAuth: true,
          title: 'Draft & Schedule & Sent',
          icon: 'file-earmark-spreadsheet',
        },
      },
      {
        path: 'people',
        name: 'people',
        component: () => import('pages/workspace/_workspaceId/people.vue'),
        meta: {
          requireAuth: true,
          title: 'People',
          icon: 'people',
        },
      },
      {
        path: 'statistic',
        name: 'statistic',
        component: () => import('pages/workspace/_workspaceId/statistic.vue'),
        meta: {
          requireAuth: true,
          title: 'Statistic',
          icon: 'bar-chart',
          separator: true,
        },
      },
      {
        path: 'C:channelId',
        name: 'channel-detail',
        component: () => import('pages/workspace/_workspaceId/_channelId.vue'),
        meta: {
          requireAuth: true,
          title: 'Channel',
          icon: 'journal-richtext',
          dynamicRouter: true,
          separator: true,
          type: 'channel',
          params: 'channelId',
        },
      },
      {
        path: 'D:memberId',
        name: 'one-to-one',
        component: () => import('pages/workspace/_workspaceId/oneToOne.vue'),
        meta: {
          requireAuth: true,
          title: 'One to One',
          icon: 'person-up',
          dynamicRouter: true,
          separator: true,
          type: 'onToOne',
          params: 'memberId',
        },
      },
      {
        path: 'calendar',
        name: 'calendar',
        component: () => import('pages/workspace/_workspaceId/unreadTasks.vue'),
        meta: {
          requireAuth: true,
          title: 'Calendar',
          icon: 'calendar-event',
        },
        children: [],
      },
    ],
  },
];

export default routesWorkspace;
