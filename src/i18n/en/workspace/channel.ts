export default {
  createChannel: 'Create new channel',
  create: 'Create',
  name: 'Name',
  description: 'Description',
  makePrivate: 'Make private',
};
