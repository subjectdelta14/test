import { api } from 'boot/axios';
import { bodyProfile, uploadAvatar } from 'src/services/user/model';
const user = '/user';
class UserService {
  profile() {
    return api.get(`${user}/profile`);
  }

  editProfile(body: bodyProfile) {
    return api.put(`${user}/profile`, body);
  }

  editAvatarProfile(body: uploadAvatar) {
    return api.post(`${user}/refresh_token`, body);
  }
}

export default new UserService();
