export default {
  loginSuccess: 'Login Success !',
  loginFailed: 'Login Failed !',
  createAccountSuccess: 'Create Account Success !',
  createAccountFailed: 'Create Account Failed !',
  resendFailed: 'Resen Otp Failed !',
};
