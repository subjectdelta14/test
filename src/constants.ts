export const KEY_ACCESS_TOKEN = 'access_token';
export const KEY_REFRESH_ACCESS_TOKEN = 'refresh_access_token';
export const TYPE_LOGIN = 'SIGN_IN';
export const TYPE_REGISTER = 'SIGN_UP';
