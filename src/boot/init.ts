import { boot } from 'quasar/wrappers';
import { useMainStore } from 'stores/main';
import { getToken } from 'src/helper/auth';
import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
const firebaseConfig: any = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  projectId: process.env.VUE_APP_PR_ID,
  storageBucket: process.env.VUE_APP_STORE_BUCKET,
  messagingSenderId: process.env.VUE_APP_MESS_SENDER_ID,
  appId: process.env.VUE_APP_FIREBASE_APP_ID,
  measurementId: process.env.VUE_APP_MEASUREMENT_ID,
};

// Initialize Firebase
const appFirebase = initializeApp(firebaseConfig);
const analytics = getAnalytics(appFirebase);
export default boot(({ app, router, redirect, store }) => {
  console.log('init');
  console.log('getToken', getToken());
  if (!!getToken()) {
    const mainStore = useMainStore();
    mainStore.getInfoUser();
  }

  // const rule : any = {
  //   email : [val]
  // }
  // app.config.globalProperties.$rule = rule
});

export { appFirebase, analytics };
