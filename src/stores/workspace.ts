import { defineStore } from 'pinia';
import WorkspaceService from 'src/services/workspace';
import NotifyServices from 'src/plugins/NotifyServices';
import { paramsMember } from 'src/services/workspace/model';
// import {useRouter} from "vue-router";
export interface State {
  listWorkspace: any;
  detailWorkspace: any;
  IsShowModalInvite: boolean;
  listMemberWorkspace: any;
}

export const useWorkspaceStore = defineStore({
  id: 'workspaceStore',
  state: () =>
    ({
      listWorkspace: [],
      detailWorkspace: null,
      IsShowModalInvite: false,
      listMemberWorkspace: [],
    } as State),

  getters: {
    getListWorkspace: (state) => {
      return state.listWorkspace;
    },
  },

  actions: {
    getAllWorkspace() {
      return new Promise((resolve, reject) => {
        WorkspaceService.getAllWorkspace()
          .then((res: any) => {
            console.log('res', res);
            if (res?.status_code === 200) {
              this.listWorkspace = res.data;
              resolve(this.listWorkspace);
            } else {
              reject(res.message);
            }
          })
          .catch((err) => {
            reject(err.message);
            NotifyServices.showMessageError(`${err.message}`);
          });
      });
    },
    getDetailWorkspace(id: string) {
      return new Promise((resolve, reject) => {
        WorkspaceService.getDetailWorkspace(id)
          .then((res: any) => {
            if (res?.status_code === 200) {
              console.log('res', res);
              this.detailWorkspace = { ...res.data };
              resolve(this.detailWorkspace);
            } else {
              console.log(res);
              reject(res);
            }
          })
          .catch((err) => {
            console.log(err);
            reject(err.message);
            NotifyServices.showMessageError(`${err.message}`);
          });
      });
    },
    checkWorkspaceWithLogin(router: any) {
      this.getAllWorkspace().then((res) => {
        if (this.listWorkspace.length > 0) {
          router
            .push({
              name: 'list-workspace',
            })
            .then();
        } else {
          router
            .push({
              name: 'home',
            })
            .then();
        }
      });
    },
    showModalInvite(boolean: boolean) {
      this.IsShowModalInvite = boolean;
    },
  },
});
