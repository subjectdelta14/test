import { boot } from 'quasar/wrappers';
import CButton from 'src/components/common/CButton.vue';
import CTextField from 'src/components/common/CTextField.vue';
import CBreadcrumbs from 'src/components/common/CBreadcrumbs.vue';
import CModal from 'src/components/common/CModal.vue';
import CSelect from 'src/components/common/CSelect.vue';
import CAvatar from 'src/components/common/CAvatar.vue';
import CDatePicker from 'src/components/common/CDatePicker.vue';
import CTextArea from 'src/components/common/CTextArea.vue';
import CUpload from 'src/components/common/CUpload.vue';

export default boot(({ app }) => {
  app.component('CBtn', CButton);
  app.component('CTextField', CTextField);
  app.component('CBreadcrumbs', CBreadcrumbs);
  app.component('CModal', CModal);
  app.component('CSelect', CSelect);
  app.component('CAvatar', CAvatar);
  app.component('CDatePicker', CDatePicker);
  app.component('CTextArea', CTextArea);
  app.component('CTextArea', CTextArea);
  app.component('CUpload', CUpload);
});
