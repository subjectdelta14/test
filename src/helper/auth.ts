import { boot } from 'quasar/wrappers';
// import store from 'src/store';
import { KEY_ACCESS_TOKEN } from 'src/constants';

// import { getToken } from "../../../../../utils/user-local"

export function getToken() {
  if (!localStorage) return false;
  if (KEY_ACCESS_TOKEN in localStorage) {
    return JSON.parse(`${localStorage.getItem(KEY_ACCESS_TOKEN) || {}}`);
  }
  return false;
}

export function setToken(data: string) {
  if (!localStorage) return;
  if (!data) {
    removeToken();
  }
  localStorage.setItem(KEY_ACCESS_TOKEN, data || '');
}

export function removeToken() {
  if (!localStorage) return;
  localStorage.removeItem(KEY_ACCESS_TOKEN);
}

export function isLogged() {
  return !!getToken();
}

export function returnLogin(context: any) {
  context.router.push({
    name: 'login',
    query: {
      urlPath: context.urlPath,
    },
  });
}

export function gotoUrlPath(context: any) {
  if (context.urlPath) {
    context.router.push(context.urlPath);
  }
}
