// This is just an example,
// so you can safely delete all default props below

export default {
  required: '{_field_} is required',
  email: 'Invalid email address format',
};
