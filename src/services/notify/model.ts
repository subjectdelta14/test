import { fcmToken } from 'src/services/auth/model';

export interface sendTest extends fcmToken {
  is_background: boolean;
}
