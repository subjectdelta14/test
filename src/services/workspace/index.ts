import { api } from 'boot/axios';
import {
  bodyInviteWorkspace,
  bodyJoinWorkspace,
  bodyWorkspace,
  paramsMember,
} from 'src/services/workspace/model';
const workspace = '/workspace';
class WorkspaceService {
  createWorkspace(body: bodyWorkspace) {
    return api.post(`${workspace}`, body);
  }

  getAllWorkspace() {
    return api.get(`${workspace}`);
  }

  getDetailWorkspace(id: string) {
    return api.get(`${workspace}/${id}`);
  }

  findAllMemberWorkspace(id: string, params: paramsMember) {
    return api.get(`${workspace}/${id}/members`, {
      params,
    });
  }
  findShareInviteWorkspace(id: string) {
    return api.get(`${workspace}/${id}/share-invite`);
  }

  editWorkspace(id: string, body: bodyWorkspace) {
    return api.put(`${workspace}/${id}`, body);
  }

  inviteWorkspace(body: bodyInviteWorkspace) {
    return api.post(`${workspace}/invite`, body);
  }

  joinWorkspace(body: bodyJoinWorkspace) {
    return api.post(`${workspace}/join`, body);
  }
}

export default new WorkspaceService();
