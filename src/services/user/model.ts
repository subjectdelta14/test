export interface bodyProfile {
  phone: string;
  full_name: string;
}

export interface uploadAvatar {
  avatar: FormData;
}
