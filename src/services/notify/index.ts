import { api } from 'boot/axios';
import { sendTest } from 'src/services/notify/model';

class ApiService {
  sendTestNotify(body: sendTest) {
    return api.post('/notification/send-test', body);
  }
}

export default new ApiService();
