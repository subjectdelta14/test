export default {
  rules: 'Attachment (limit 5 files; 25mb each file)',
  totalUploaded: 'files uploaded',
};
