import { RouteRecordRaw } from 'vue-router';
import routesWorkspace from 'src/router/workspace';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/',
    name: 'home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/',
        name: 'home-workspace',
        component: () => import('pages/home/HomeWorkspace.vue'),
        meta: {
          requireAuth: true,
        },
      },
      {
        path: '/components',
        name: 'components',
        component: () => import('pages/ComponentsPage.vue'),
      },
    ],
  },
  {
    path: '/workspace/:workspaceId/invite',
    component: () => import('layouts/MainSecond.vue'),
    // ?code=1dbb3fe4-1c94-4caa-b29c-110398f77ce1
    children: [
      {
        path: '',
        name: 'invite',
        component: () => import('pages/home/InviteWs.vue'),
        meta: {
          requireAuth: true,
        },
      },
    ],
  },
  {
    path: '/preview',
    redirect: '/preview/:workspaceId/invite',
    component: () => import('layouts/MainSecond.vue'),
    // ?code=1dbb3fe4-1c94-4caa-b29c-110398f77ce1
    children: [
      {
        path: 'create-workspace',
        name: 'create-workspace',
        component: () => import('pages/home/CreateWorkspace.vue'),
        meta: {
          requireAuth: true,
        },
      },
      {
        path: 'list-workspace',
        name: 'list-workspace',
        component: () => import('pages/home/ListWorkSpace.vue'),
        meta: {
          requireAuth: true,
        },
      },
    ],
  },
  {
    path: '/auth',
    redirect: '/auth/login',
    component: () => import('layouts/LayoutLogin.vue'),
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import('src/pages/auth/LoginHome.vue'),
      },
      {
        path: 'create-account',
        name: 'create-account',
        component: () => import('src/pages/auth/CreateAccount.vue'),
      },
      {
        path: 'verify',
        name: 'verify',
        component: () => import('src/pages/auth/VerifyLogin.vue'),
      },
    ],
  },
  ...routesWorkspace,
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
