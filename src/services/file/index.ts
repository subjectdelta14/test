import { api } from 'boot/axios';
// import { uploadFile } from 'src/services/file/model';

class FileService {
  uploadFile(body: any) {
    return api.post('/file', body);
  }
}

export default new FileService();
