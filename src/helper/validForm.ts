export const checkEmail = (text: string) => {
  // const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const re = /^[a-zA-Z0-9._]+@\S+\.(\S+)$/;
  // const re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  return re.test(String(text).toLowerCase());
};

// register
export const checkUserName = (text: string) => {
  const re = /^[a-zA-Z0-9_]{5,25}$/;
  return re.test(String(text));
};
export const checkCompanyName = (text: string) => {
  const re = /^[a-zA-Z0-9\s\\&\\.-]{1,100}$/;
  return re.test(String(text));
};
// RepresentativeName, FistName,LastName
export const checkName = (text: string) => {
  const re = /^[a-zA-Z ]{1,25}$/;
  return re.test(String(text));
};
export const checkLastName = (text: string) => {
  const re = /^[a-zA-Z ]{1,25}$/;
  return re.test(String(text));
};

// export const checkDateOfBirth = (text:string) => {
//     const re = /^[0-9]{2}\\[0-9]{2}\\[0-9]{4}$/;
//     return re.test(String(text).toLowerCase());
// }
export const checkAddress = (text: string) => {
  const re = /^[a-zA-Z0-9 \s&.\/\-]{1,200}$/;
  return re.test(String(text));
};

export const checkCredentialId = (text: string) => {
  const re = /^[a-zA-Z0-9 ]{1,50}$/;
  return re.test(String(text));
};

export const checkSchool = (text: string) => {
  const re = /^[a-zA-Z0-9 ]{1,100}$/;
  return re.test(String(text));
};
export const checkGrade = (text: string) => {
  const re = /^[0-9/.]{1,100}$/;
  return re.test(String(text));
};

export const checkPhone = (text: string) => {
  const re = /^[0-9]{1,12}$/;
  return re.test(String(text));
};
//^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$
export const checkPassword = (text: string) => {
  const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
  return re.test(String(text));
};

//LinkedIn URL /Twitter URL /Facebook URL /Instagram URL
export const checkURL = (text: string) => {
  const re =
    /^[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;
  return re.test(String(text));
};

export const checkContenteditable = (text: string) => {
  const re = /\&nbsp;|<\/?[^>]+(>|$)/g;
  const newText = text.replace(/\&nbsp;/g, ' ');
  return newText.replace(re, '').length;
};

export const checkOtpNumber = (text: string) => {
  const re = /^[0-9]$/;
  return re.test(String(text));
};

export const checkOtpNumber6Dig = (text: string) => {
  const re = /^[0-9]{6}$/;
  return re.test(String(text));
};
