FROM node:16.18.0 as build-stage

WORKDIR /app

COPY . .

RUN yarn
RUN yarn build

FROM nginx:latest as production-stage

COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
COPY ./docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
