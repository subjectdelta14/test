#!/bin/bash

docker system prune -a -f
git pull origin dev
cd docker && docker-compose up -d --build
