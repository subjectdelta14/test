import { defineStore } from 'pinia';
import ChannelService from 'src/services/channel';
// import NotifyServices from 'src/plugins/NotifyServices';
import { paramsMember, paramsFiles, bodyChannel } from 'src/services/channel/model';
import NotifyServices from 'src/plugins/NotifyServices';
// import {useRouter} from "vue-router";
export interface State {
  listChannel: any;
  listOneToOne: any;
  IsShowModal: boolean;
  detailChanel: any;
}

export const useChannelStore = defineStore({
  id: 'channelStore',
  state: () =>
    ({
      listChannel: [],
      listOneToOne: [],
      detailChanel: null,
      IsShowModal: false,
    } as State),

  getters: {},

  actions: {
    getChannelInWs(workspaceId: string) {
      return new Promise((resolve, reject) => {
        ChannelService.getAllChannel(workspaceId)
          .then((res: any) => {
            console.log('res', res);
            if (res?.status_code === 200) {
              console.log(res.data);
              this.listChannel = [];
              this.listOneToOne = [];
              res.data.map((item: any) => {
                if (['GENERAL', 'MANUAL'].includes(item.type)) {
                  this.listChannel.push(item);
                }
                if (['ONE_TO_ONE'].includes(item.type)) {
                  this.listOneToOne.push(item);
                }
              });
              // this.listChannel = res.data;
              resolve(res.data);
            } else {
              reject(res.message);
            }
          })
          .catch((err: any) => {
            reject(err.message);
            // NotifyServices.showMessageError(`${err.message}`);
          });
      });
    },
    getChannelDetail(id: string) {
      return new Promise((resolve, reject) => {
        ChannelService.getDetailChannel(id)
          .then((res: any) => {
            if (res?.status_code === 200) {
              this.detailChanel = { ...res.data };
              resolve(res.data);
            } else {
              reject(res.message);
            }
          })
          .catch((err: any) => {
            reject(err.message);
            // NotifyServices.showMessageError(`${err.message}`);
          });
      });
    },
    createChannel(body: bodyChannel) {
      this.listChannel.push(body);
    },
    showModalAddChannel(boolean: boolean) {
      this.IsShowModal = boolean;
    },
    showModalEditChannel(boolean: boolean) {
      console.log(1);
      this.IsShowModal = boolean;
    },
  },
});
