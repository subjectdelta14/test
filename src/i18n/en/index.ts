import valid from 'src/i18n/en/valid';
import auth from 'src/i18n/en/auth';
import workspace from 'src/i18n/en/workspace';
import upload from 'src/i18n/en/upload';
export default {
  valid,
  auth,
  workspace,
  upload,
};
