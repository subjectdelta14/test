export default {
  joinWsSuccess: 'Invite success',
  joinWsFailed: 'Invite failed',
  invitePeopleSuccess: 'Invite success',
  invitePeopleFailed: 'Invite failed',
  getPeopleFailed: 'Get members failed',
  createChannel: 'Create channel success',
};
