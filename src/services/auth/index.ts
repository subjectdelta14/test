import { api, axiosNoAuth } from 'boot/axios';
import {
  bodyLogin,
  bodyLoginSocial,
  fcmToken,
  bodyRefreshToken,
  bodyRegister,
  bodyOtp,
} from 'src/services/auth/model';
const auth = '/auth';
class AuthService {
  login(body: bodyLogin) {
    return axiosNoAuth.post(`${auth}/login`, body);
  }

  loginSocial(body: bodyLoginSocial) {
    return axiosNoAuth.post(`${auth}/social`, body);
  }

  refreshToken(body: bodyRefreshToken) {
    return api.post(`${auth}/refresh-token`, body);
  }

  register(body: bodyRegister) {
    return axiosNoAuth.post(`${auth}/register`, body);
  }

  otp(body: bodyOtp) {
    return axiosNoAuth.post(`${auth}/otp`, body);
  }

  logout(body: fcmToken) {
    return api.post(`${auth}/logout`, body);
  }
}

export default new AuthService();
