export const getItem = (
  array: Array<any>,
  key: string,
  value: any,
  objectDefault: object,
) => {
  // array data
  // key de check
  // value de check
  // objectDefault neu ko tim duoc ket qua se lay object nay de return
  const filtered = array.find((item) => item[key] === value);
  return filtered ? filtered : objectDefault || array[0];
};

export const checkData = (object: object | any, listKey?: Array<string>) => {
  // object de check data
  // listKey 1 array chua cac key de check data
  let check = false;
  let returnCheck = false;
  if (listKey) {
    listKey.forEach((key) => {
      if (!returnCheck) {
        check = !!object[key];
      }
      returnCheck = !check;
    });
  } else {
    for (const key in object) {
      if (!returnCheck) {
        check = !!object[key];
      }
      returnCheck = !check;
    }
  }
  return check;
};
// import chroma from 'chroma-js'

const colors = ['#0078F0', '#FFAC14', '#6342FF', '#2EB553', '#FF5151', '#1ABBC1'];

function hashCode(s: string) {
  let h: any;
  for (let i = 0; i < s.length; i++) h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;

  return h;
}

export const genColor = (id: string) => {
  if (!id) return { bgColor: colors[2], textColor: 'white' };
  const code = Math.abs(hashCode(id) % colors.length);
  const color = colors[code];
  // const textColor = chroma.contrast(color, 'white') > 2 ? 'white' : '#1A2948'
  const textColor = color;
  const bgColor = color + '22';
  return { bgColor, textColor };
};
