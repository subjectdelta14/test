export interface bodyWorkspace {
  name: string;
  description?: string;
  thumbnail_id?: number | null;
}

export interface bodyInviteWorkspace {
  emails: Array<string>;
  workspace_id: number;
}

export interface bodyJoinWorkspace {
  invite_code: string;
}

export interface paramsMember {
  page: number;
  page_size: number;
  sort_by?: string;
  order_by?: string;
  search?: string;
}
