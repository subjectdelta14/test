export default {
  invitePeople: 'Invite people to this workspace',
  createChannel: 'Create a channel',
  payment: 'Payment',
  admin: 'Administration',
  preferences: 'Preferences',
  addWorkspace: 'Add workspaces',
  switchWorkspace: 'Switch workspaces',
  signOutWorkspace: 'Sign out of this workspace',
};
