import menuDetail from 'src/i18n/en/workspace/menuDetail';
import peoples from 'src/i18n/en/workspace/peoples';
import notify from 'src/i18n/en/workspace/notify';
import channel from 'src/i18n/en/workspace/channel';
export default {
  menuDetail,
  peoples,
  notify,
  channel,
};
