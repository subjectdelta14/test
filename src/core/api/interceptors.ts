import { getToken, removeToken, returnLogin } from 'src/helper/auth';
import { useMainStore } from 'stores/main';

function configRequest(instance: any, context: any) {
  const mainStore = useMainStore(context.store);
  instance.interceptors.request.use(
    function (config: any) {
      if (!!getToken()) {
        const token = getToken();
        if (token && token.expired_time - 60 <= Date.now()) {
          mainStore.refreshToken(token.refresh_token);
        }
        if (token) {
          config.headers.Authorization = 'Bearer ' + token?.token || '';
        }
      } else {
        mainStore.logout();
        returnLogin(context);
      }
      return config;
    },
    function (err: any) {
      return Promise.reject(err);
    },
  );
}

function configResponse(instance: any, context: any) {
  const mainStore = useMainStore(context.store);
  instance.interceptors.response.use(
    (response: any) => {
      console.log(response.data);
      if (
        response.data.status_code === 401 &&
        context.router.currentRoute.value.name !== 'login'
      ) {
        mainStore.logout();
        returnLogin(context);
      }
      return response.data as any;
    },
    (error: any) => {
      if (error.response) {
        if (
          (error.response.status === 401 || error.response.status === 403) &&
          !window.location.pathname.includes('login')
        ) {
          mainStore.logout();
          returnLogin(context);
        }
        console.log('error.response', error.response);
        const errorData = error.response.data;
        return Promise.reject({
          ...errorData,
        });
      }
    },
  );
}

export default {
  configRequest,
  configResponse,
};

// Thành công:
// {
//   "data": {},
//   "message" : "", -> tự xử lý FE. mapping theo file message. -> Phương sẽ xử lý
//   "status_code": 200 -> thành công
// }
//
// Thất bại:
// {
//   "errors" : {
//
//   "username" : "Username error",
//     ""
// }, -> form, query nhiều trường.
//   "status_code": 406 (!=  200),
//   "message": ""
// }
